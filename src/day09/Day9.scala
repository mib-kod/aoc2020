package day09

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day9 extends App {
  println("Day 9")
  val data = Files.readAllLines(Paths.get("src/day09/input.txt")).asScala.toSeq.map(_.toLong)
  val Preamble = 25

  for (x <- data.sliding(Preamble+1)) {
    val checksum = x.take(Preamble)
    val n = x.last
    if (!checksum.combinations(2).exists(_.sum == n)) {
      println("Invalid " + n)    // 248131121
    }
  }
}

object Day9PartTwo extends App {
  println("Day 9 Part Two")

  val Crypto = 248131121

  val data = Files.readAllLines(Paths.get("src/day09/input.txt")).asScala.toSeq.map(_.toLong)

  var found = false
  for (i <- data.tails if !found) {
    var sum = 0L
    var len = 0
    for (j <- i if sum < Crypto) {
      sum += j
      len += 1
    }

    if (sum == Crypto) {
      val seq = i.take(len)
      println("Sequence: " + seq)
      println("Min+Max: " + (seq.min + seq.max))
      found = true
    }
  }
}
