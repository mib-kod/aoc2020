package day11

import java.nio.file.{Files, Paths}
import scala.annotation.tailrec
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day11 extends App {
  println("Day 11")
  var data: IndexedSeq[IndexedSeq[Char]] = Files.readAllLines(Paths.get("src/day11/input.txt")).asScala.toIndexedSeq.map(_.toIndexedSeq)

  println(data.map(_.mkString).mkString("\n"))

  val grid = Array.ofDim[Char](data.length, data.head.length)

  def seat(x: Int, y: Int): String = {
    if (x < 0 || x >= grid.head.length || y < 0 || y >= grid.length) ""
    else data(y)(x).toString
  }

  def seatNext(x: Int, y: Int): Char = {
    val adjacent =
      seat(x-1,y-1) + seat(x,y-1) + seat(x+1,y-1) +
      seat(x-1,y)                 + seat(x+1,y) +
      seat(x-1,y+1) + seat(x,y+1) + seat(x+1,y+1)

    data(y)(x) match {
      case 'L' if adjacent.forall(_ != '#') => '#'
      case '#' if adjacent.count(_ == '#') >= 4 => 'L'
      case x => x
    }
  }

  var stable = false
  while (!stable) {
    for (y <- grid.indices; x <- grid.head.indices) {
      grid(y)(x) = seatNext(x, y)
    }
    val next = grid.map(_.toIndexedSeq).toIndexedSeq
    println("----------------------------------")
    println(next.map(_.mkString).mkString("\n"))
    stable = data == next
    data = next
  }

  val occupied = data.flatten.count(_ == '#')
  println(occupied)
}

object Day11PartTwo extends App {
  println("Day 11 Part Two")
  var data: IndexedSeq[IndexedSeq[Char]] = Files.readAllLines(Paths.get("src/day11/input.txt")).asScala.toIndexedSeq.map(_.toIndexedSeq)

  println(data.map(_.mkString).mkString("\n"))

  val grid = Array.ofDim[Char](data.length, data.head.length)

  @tailrec def seatAtDirection(x: Int, y: Int, dx: Int, dy: Int): String = {
    if (x < 0 || x >= grid.head.length || y < 0 || y >= grid.length) ""
    else {
      val s = data(y)(x)
      if (s == '.') seatAtDirection(x + dx, y + dy, dx, dy)
      else s.toString
    }
  }

  def seatNext(x: Int, y: Int): Char = {
    def search(dx: Int, dy: Int) = seatAtDirection(x+dx, y+dy, dx, dy)
    val nearby =
      search(-1,-1) + search(0,-1) + search(1,-1) +
      search(-1, 0)                + search(1, 0) +
      search(-1, 1) + search(0, 1) + search(1, 1)

    data(y)(x) match {
      case 'L' if nearby.forall(_ != '#') => '#'
      case '#' if nearby.count(_ == '#') >= 5 => 'L'
      case x => x
    }
  }

  var stable = false
  while (!stable) {
    for (y <- grid.indices; x <- grid.head.indices) {
      grid(y)(x) = seatNext(x, y)
    }
    val next = grid.map(_.toIndexedSeq).toIndexedSeq
    println("----------------------------------")
    println(next.map(_.mkString).mkString("\n"))
    stable = data == next
    data = next
  }

  val occupied = data.flatten.count(_ == '#')
  println(occupied)
}
