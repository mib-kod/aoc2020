package day08

import java.nio.file.{Files, Paths}
import scala.collection.mutable
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day8 extends App {
  println("Day 8")
  val data = Files.readAllLines(Paths.get("src/day08/input.txt")).asScala.toVector

  var pc = 0
  var acc = 0
  val visited = new mutable.BitSet()

  while (!visited.contains(pc)) {
    visited.add(pc)
    val s"$inst $arg" = data(pc)
    println(s"$pc $acc $inst $arg")
    inst match {
      case "nop" => pc +=1
      case "acc" => acc += arg.toInt; pc += 1
      case "jmp" => pc += arg.toInt
    }
  }
}

object Day8PartTwo extends App {
  println("Day 8 Part Two")

  val data = Files.readAllLines(Paths.get("src/day08/input.txt")).asScala.toVector

  var finished = false

  for (i <- data.indices if !finished) {
    println("*"+i)
    val prog = data.toArray
    if (prog(i).startsWith("nop")) prog(i) = "jmp" + prog(i).drop(3)
    else if (prog(i).startsWith("jmp")) prog(i) = "nop" + prog(i).drop(3)

    var pc = 0
    var acc = 0
    val visited = new mutable.BitSet()

    while (!visited.contains(pc) && !finished) {
      visited.add(pc)
      val s"$inst $arg" = prog(pc)
      inst match {
        case "nop" => pc += 1
        case "acc" => acc += arg.toInt; pc += 1
        case "jmp" => pc += arg.toInt
      }

      if (pc == prog.length) {
        println
        println(s"acc=$acc")
        finished = true
      }
    }
  }
}
