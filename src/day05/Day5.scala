package day05

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters._

object Day5 extends App {
  println("Day 5")
  val data = Files.readAllLines(Paths.get("src/day05/input.txt")).asScala.toSeq

  def toSeatId(seat: String): Int = {
    val row = Integer.parseInt(seat.take(7).replace("F","0").replace("B","1"), 2)
    val column = Integer.parseInt(seat.drop(7).replace("L","0").replace("R","1"), 2)
    row * 8 + column
  }

  println(data.map(toSeatId).max)
}

object Day5PartTwo extends App {
  println("Day 5 Part Two")

  val data = Files.readAllLines(Paths.get("src/day05/input.txt")).asScala.toSeq

  def toSeatId(seat: String): Int = {
    val row = Integer.parseInt(seat.take(7).replace("F","0").replace("B","1"), 2)
    val column = Integer.parseInt(seat.drop(7).replace("L","0").replace("R","1"), 2)
    row * 8 + column
  }

  val taken = data.map(toSeatId).toSet
  val free = Range.inclusive(taken.min, taken.max).toSet -- taken
  println(free)
}
