package day14

import java.nio.file.{Files, Paths}
import scala.collection.mutable
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day14 extends App {
  println("Day 14")
  val data = Files.readAllLines(Paths.get("src/day14/input.txt")).asScala.toSeq

  val memory = new mutable.HashMap[Int, Long]()
  var clearMask = 0L
  var setMask = 0L

  for (line <- data) line match {
    case s"mask = $mask" => println(mask)
      clearMask = ~java.lang.Long.parseLong(mask.replace("1","X").replace("0","1").replace("X","0"), 2)
      setMask   =  java.lang.Long.parseLong(mask.replace("X","0"), 2)
    case s"mem[$addr] = $value" =>
      println(addr, value.toLong, (value.toLong | setMask) & clearMask)
      memory.put(addr.toInt, (value.toLong | setMask) & clearMask)
  }

  println(memory.values.sum)
}

object Day14PartTwo extends App {
  println("Day 14 Part Two")
  val data = Files.readAllLines(Paths.get("src/day14/input.txt")).asScala.toSeq

  val memory = new mutable.HashMap[Long, Long]()

  case class Mask(clear: Long, set: Long)
  var masks = List.empty[Mask]

  def parseMask(s: String) = Mask(
    clear = ~java.lang.Long.parseLong(s.replace("1","X").replace("0","1").replace("X","0"), 2),
    set   =  java.lang.Long.parseLong(s.replace("X","0"), 2)
  )

  def maskCombos(b: List[Char], remaining: List[Char]): List[Mask] = remaining match {
    case Nil => parseMask(b.mkString.reverse) :: Nil
    case '0' :: rest => maskCombos('X' :: b, rest)
    case '1' :: rest => maskCombos('1' :: b, rest)
    case 'X' :: rest => maskCombos('0' :: b, rest) ::: maskCombos('1' :: b, rest)
  }

  for (line <- data) line match {
    case s"mask = $mask" =>
      masks = maskCombos(Nil, mask.toList)
    case s"mem[$addr] = $value" =>
      for (mask <- masks)
        memory.put((addr.toLong | mask.set) & mask.clear, value.toLong)
  }
  println(memory.values.sum)
}
