package day16

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day16 extends App {
  println("Day 16")
  val data = Files.readAllLines(Paths.get("src/day16/input.txt")).asScala.toIndexedSeq

  var rulesData = data.takeWhile(_.nonEmpty)
  val yours = data(data.indexOf("your ticket:")+1)
  val nearby = data.drop(data.indexOf("nearby tickets:")+1)

  case class Rule(name: String, r1: Range, r2: Range) {
    def isValid(value: Int): Boolean =
      r1.contains(value) || r2. contains(value)
  }

  val rules = rulesData.map { l =>
    val s"$rule: $r1min-$r1max or $r2min-$r2max" = l
    Rule(rule, Range.inclusive(r1min.toInt, r1max.toInt), Range.inclusive(r2min.toInt, r2max.toInt))
  }

  println(rules)

  var sum = 0
  for (n <- nearby) {
    val values = n.split(',').map(_.toInt).toSeq
    for (v <- values if !rules.exists(_.isValid(v)))
      sum += v
  }
  println(sum)
}

object Day16PartTwo extends App {
  println("Day 16 Part Two")

  val data = Files.readAllLines(Paths.get("src/day16/input.txt")).asScala.toIndexedSeq

  var rulesData = data.takeWhile(_.nonEmpty)
  val yours = data(data.indexOf("your ticket:")+1)
  val nearby = data.drop(data.indexOf("nearby tickets:")+1)

  class Rule(val name: String, r1: Range, r2: Range) {
    var positive: Set[Int] = Set.empty
    var negative: Set[Int] = Set.empty
    var field: Int = -1

    def isValid(value: Int): Boolean =
      r1.contains(value) || r2. contains(value)

    def checkValid(value: Int, fieldNo: Int): Unit =
      if (isValid(value)) positive += fieldNo else negative += fieldNo

    def candidates: Set[Int] = if (field != -1) Set.empty else positive -- negative
    override def toString = s"$name: ${r1.min}-${r1.max} or ${r2.min}-${r2.max} ($candidates) $field"
  }

  val rules = rulesData.map { l =>
    val s"$rule: $r1min-$r1max or $r2min-$r2max" = l
    new Rule(rule, Range.inclusive(r1min.toInt, r1max.toInt), Range.inclusive(r2min.toInt, r2max.toInt))
  }

  val valid = nearby.filter { n =>
    val values = n.split(',').map(_.toInt).toSeq
    values.forall(v => rules.exists(_.isValid(v)))
  }

  for (n <- valid) {
    val values = n.split(',').map(_.toInt).toSeq
    for ((v, idx) <- values.zipWithIndex)
      rules.foreach(_.checkValid(v, idx))
  }

  println(rules.mkString("\n"))

  while (rules.exists(_.field == -1)) {
    for (pos <- rules.indices) {
      rules.toList.filter(_.candidates.contains(pos)) match {
        case single :: Nil =>
          single.field = pos
        case _ =>
      }
    }
  }

  println(rules.mkString("\n"))

  val yoursTicket = yours.split(',').map(_.toLong).toIndexedSeq

  val answer = rules
    .filter(_.name.startsWith("departure"))
    .map(rule => yoursTicket(rule.field))
    .product

  println(answer)
}
