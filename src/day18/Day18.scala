package day18

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day18 extends App {
  println("Day 18")
  val data = Files.readAllLines(Paths.get("src/day18/input.txt")).asScala.toIndexedSeq

  def plus(cs: List[Char], acc: Long = 0): Long = cs match {
    case Nil => acc
    case '(' :: rest  =>
      val (iParentes, rest2) = p(rest)
      plus(rest2, acc + plus(iParentes))
    case '+' :: rest => plus(rest, acc)
    case '*' :: rest => mult(rest, acc)
    case digit :: rest => plus(rest, acc + digit.toString.toInt)
  }

  def mult(cs: List[Char], acc: Long = 0): Long = cs match {
    case Nil => acc
    case '(' :: rest  =>
      val (iParentes, rest2) = p(rest)
      mult(rest2, acc * plus(iParentes))
    case '+' :: rest => plus(rest, acc)
    case '*' :: rest => mult(rest, acc)
    case digit :: rest => mult(rest, acc * digit.toString.toInt)
  }

  def p(cs: List[Char], acc: List[Char] = Nil, depth: Int = 0): (List[Char], List[Char]) = cs match {
    case ')' :: rest if depth == 0 => (acc.reverse, rest)
    case ')' :: rest => p(rest, ')' :: acc, depth-1)
    case '(' :: rest => p(rest, '(' :: acc, depth+1)
    case c :: rest => p(rest, c :: acc, depth)
  }

  def eval(line: String): Long =
    plus(line.toList.filterNot(_ == ' '))

  val results: Seq[Long] = data map eval
  println(results.sum)
}

object Day18PartTwo extends App {
  println("Day 18 Part Two")

  val data = Files.readAllLines(Paths.get("src/day18/input.txt")).asScala.toIndexedSeq

  def plus(cs: List[Char], acc: Long = 0): Long = cs match {
    case Nil => acc
    case '(' :: rest  =>
      val (iParentes, rest2) = p(rest)
      plus(rest2, acc + plus(iParentes))
    case '+' :: rest => plus(rest, acc)
    case '*' :: rest => mult(rest, acc)
    case digit :: rest => plus(rest, acc + digit.toString.toInt)
  }

  def mult(cs: List[Char], acc: Long = 0): Long = cs match {
    case Nil => acc
    case '(' :: rest  =>
      val (iParentes, rest2) = p(rest)
      acc * mult(rest2, plus(iParentes))
    case '+' :: rest => plus(rest, acc)
    case '*' :: rest => mult(rest, acc)
    case digit :: rest => acc * plus(rest, digit.toString.toInt)
  }

  def p(cs: List[Char], acc: List[Char] = Nil, depth: Int = 0): (List[Char], List[Char]) = cs match {
    case ')' :: rest if depth == 0 => (acc.reverse, rest)
    case ')' :: rest => p(rest, ')' :: acc, depth-1)
    case '(' :: rest => p(rest, '(' :: acc, depth+1)
    case c :: rest => p(rest, c :: acc, depth)
  }

  def eval(line: String): Long =
    plus(line.toList.filterNot(_ == ' '))

  val results: Seq[Long] = data map eval
  println(results)
  println(results.sum)   // 118088713949954 Too low   (136824720421264 correct)
}
