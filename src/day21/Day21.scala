package day21

import java.nio.file.{Files, Paths}
import scala.collection.immutable.ListSet
import scala.jdk.CollectionConverters.CollectionHasAsScala

/*
mxmxvkd kfcds sqjhc nhms                       (contains dairy  fish
mxmxvkd                  trh fvjkl sbzzf       (contains dairy
              sqjhc          fvjkl             (contains              soy
mxmxvkd       sqjhc                sbzzf       (contains        fish
-------------------------------------------------------------------------------------------
        kfcds sqjhc nhms                       (contains        fish
                         trh fvjkl sbzzf       (contains
              sqjhc          fvjkl             (contains              soy
              sqjhc                sbzzf       (contains        fish
-------------------------------------------------------------------------------------------
        kfcds       nhms                       (contains
                         trh fvjkl sbzzf       (contains
                             fvjkl             (contains              soy
                                   sbzzf       (contains
-------------------------------------------------------------------------------------------
        kfcds       nhms                       (contains
                         trh       sbzzf       (contains
                                               (contains
                                   sbzzf       (contains
*/
object Day21 extends App {
  println("Day 21")
  val data = Files.readAllLines(Paths.get("src/day21/input.txt")).asScala.toSeq

  case class Food(ingredients: Set[String], allergens: Set[String]) {
    override def toString = "Food: " + ingredients.mkString(", ") + " - " + allergens.mkString(", ")
    def check(translation: String, allergen: String) {
      if (allergens.contains(allergen))
        require(ingredients.contains(translation))
    }
  }

  val foods: Seq[Food] = for {
    l <- data
    s"$ingredientsS (contains $allergensS)" = l
    ingredients = ingredientsS.split(' ').to(ListSet)
    allergens = allergensS.split(", ").to(ListSet)
  } yield {
    Food(ingredients, allergens)
  }

  println(foods.flatMap(_.ingredients).distinct)
  println(foods.flatMap(_.ingredients).distinct.length)
  println(foods.flatMap(_.allergens).distinct)
  println(foods.flatMap(_.allergens).distinct.length)

  println("==============================================================")
  println(foods.map(_.ingredients.size).sum)
  println(foods.mkString("\n"))

  // FEL 2857  too high

  val all: Seq[(String, String)] = for {
    f <- foods
    i <- f.allergens.iterator
    a <- f.ingredients.iterator
  } yield (i, a)


  def findUnqiue(): Map[String, Seq[String]] = {
    val all: Seq[(String, String)] = for {
      f <- foods
      i <- f.ingredients.iterator
      a <- f.allergens.iterator
    } yield (i, a)
    val occurrences = all.groupMapReduce(identity)(_ => 1)(_ + _).toSeq.sortBy(_._2).reverse
    println(occurrences)
    println(occurrences.size)
    val possible = occurrences.collect { case (k, v) if v > 5 => k }
    possible.groupBy(_._2).map { case (k,v) => k -> v.map(_._1).distinct }
  }

  val translations: Map[String, Seq[String]] = findUnqiue()
  println(translations.valuesIterator.map(_.length.toLong).product)
  println(translations.mkString("\n"))
  val ts = search()
  println(ts)
  val noAllergens = foods.flatMap(_.ingredients.filterNot(ts.toSet))
  println(noAllergens)
  println("Svaret på fråga A är: " + noAllergens.length)
  println("Svaret på fråga B är: " + ts.mkString(","))

  def search(): Seq[String] = {
    var i = 0
    for {
      fish <- translations("fish").take(3)
      nuts <- translations("nuts").take(3)
      dairy <- translations("dairy").take(3)
      wheat <- translations("wheat").take(3)
      sesame <- translations("sesame").take(3)
      peanuts <- translations("peanuts").take(3)
      eggs <- translations("eggs").take(3)
      shellfish <- translations("shellfish").take(3)
      ts = Seq(dairy, eggs, fish, nuts, peanuts, sesame, shellfish, wheat).distinct
      if ts.length == 8
    } try {
      i += 1
      if (i % 1000 == 0) println(i)
      for (f <- foods) {
        f.check(fish, "fish")
        f.check(nuts, "nuts")
        f.check(dairy, "dairy")
        f.check(wheat, "wheat")
        f.check(sesame, "sesame")
        f.check(peanuts, "peanuts")
        f.check(eggs, "eggs")
        f.check(shellfish, "shellfish")
      }
      return ts
    } catch { case _: Exception => }
    ???
  }
}
