package day06

import java.nio.file.{Files, Paths}

object Day6 extends App {
  println("Day 6")
  val data = Files.readString(Paths.get("src/day06/input.txt")).split("\n\n")

  def yesPerGroup(s: String): Int =
    s.replace("\n","").distinct.length

  println(data.map(yesPerGroup).sum)
}

object Day6PartTwo extends App {
  println("Day 6 Part Two")

  val data = Files.readString(Paths.get("src/day06/input.txt")).split("\n\n")

  def allYesPerGroup(s: String): Int = {
    val qs = s.replace("\n","").distinct
    val ps = s.linesIterator.toSeq
    qs.count(q => ps.forall(_.contains(q)))
  }

  println(data.map(allYesPerGroup).sum)
}
