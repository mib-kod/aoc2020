package day10

import java.nio.file.{Files, Paths}
import scala.collection.mutable
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day10 extends App {
  println("Day 10")
  val data = Files.readAllLines(Paths.get("src/day10/input.txt")).asScala.toSeq.map(_.toInt)
  var adapters = mutable.Queue(data.sorted:_*)
  val max = data.max

  var ones = 0
  var threes = 1

  var current = 0
  while (current < data.max) {
    adapters.dropWhileInPlace(_ <= current)
    val next = adapters.dequeue()
    val diff = next - current
    if (diff == 1) ones += 1
    if (diff == 3) threes += 1
    println(s"$next - $current = $diff")
    current = next
  }

  println(s"ones: $ones, threes: $threes (${ones*threes} multiplied)")
}

object Day10PartTwo extends App {
  println("Day 10 Part Two")

  val data = Files.readAllLines(Paths.get("src/day10/input.txt")).asScala.toSeq.map(_.toInt)
  var adapters = data.sorted.toList
  val max = data.max

  def loop(path: List[Int], remain: List[Int]): List[List[Int]] = {
    val current = path.head
    if (path.head == max) List(path)
    else remain match {
      case a1 :: a2 :: a3 :: rest if a3 <= (current + 3) => loop(a1 :: path, remain.drop(1)) ::: loop(a2 :: path, remain.drop(2)) ::: loop(a3 :: path, remain.drop(3))
      case a1 :: a2 :: rest  if a2 <= (current + 3) => loop(a1 :: path, remain.drop(1)) ::: loop(a2 :: path, remain.drop(2))
      case a1 :: rest if a1 <= (current + 3) => loop(a1 :: path, remain.drop(1))
    }
  }

  val cache = new mutable.HashMap[Int,Long]()

  def loopCount(current: Int, remain: List[Int]): Long = {
    cache.getOrElseUpdate(current, {
      if (current == max) { 1 }
      else remain match {
        case a1 :: a2 :: a3 :: rest if a3 <= (current + 3) => loopCount(a1, remain.drop(1)) + loopCount(a2, remain.drop(2)) + loopCount(a3, remain.drop(3))
        case a1 :: a2 :: rest  if a2 <= (current + 3) => loopCount(a1, remain.drop(1)) + loopCount(a2, remain.drop(2))
        case a1 :: rest if a1 <= (current + 3) => loopCount(a1, remain.drop(1))
      }
    })
  }

  println(adapters.length)
  val result = loopCount(0, adapters)
  println(result)
}
