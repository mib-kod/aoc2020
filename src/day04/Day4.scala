package day04

import java.nio.file.{Files, Paths}

object Day4 extends App {
  println("Day 4")
  val data = Files.readString(Paths.get("src/day04/input.txt")).split("\n\n")

  val required = Set(" byr:", " iyr:", " eyr:", " hgt:", " hcl:", " ecl:", " pid:")

  def isValid(s: String): Boolean = {
    val s2 = " " + s.replace('\n', ' ')
    required.forall(s2.contains)
  }

  println(data.count(isValid))
}

object Day4PartTwo extends App {
  println("Day 4 Part Two")

  val data = Files.readString(Paths.get("src/day04/input.txt")).split("\n\n")

  val required = Set(" byr:", " iyr:", " eyr:", " hgt:", " hcl:", " ecl:", " pid:")

  def isValidEntry(e: String): Boolean = {
    val s"$k:$v" = e
    k match {
      case "byr" => v.toIntOption.exists(Range.inclusive(1920,2002).contains)
      case "iyr" => v.toIntOption.exists(Range.inclusive(2010,2020).contains)
      case "eyr" => v.toIntOption.exists(Range.inclusive(2020,2030).contains)
      case "hgt" => v.endsWith("cm") && v.dropRight(2).toIntOption.exists(Range.inclusive(150,193).contains) ||
                    v.endsWith("in") && v.dropRight(2).toIntOption.exists(Range.inclusive(59,76).contains)
      case "hcl" => v.matches("""#[\da-f]{6}""")
      case "ecl" => Set("amb","blu","brn","gry","grn","hzl","oth").contains(v)
      case "pid" => v.matches("""\d{9}""")
      case "cid" => true
    }
  }

  def isValid(s: String): Boolean = {
    val s2 = " " + s.replace('\n', ' ')
    required.forall(s2.contains) && s2.split(' ').map(_.trim).filter(_.nonEmpty).forall(isValidEntry)
  }

  println(data.count(isValid))
}
