package day17

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day17 extends App {
  println("Day 17")
  val data = Files.readAllLines(Paths.get("src/day17/input.txt")).asScala.toIndexedSeq

  case class Cube(x: Int, y: Int, z: Int) {
    def neighbors: Seq[Cube] =
      for {
        x2 <- x-1 to x+1
        y2 <- y-1 to y+1
        z2 <- z-1 to z+1
        if !(x2 == x && y2 == y && z2 == z)
      } yield Cube(x2, y2, z2)
  }

  val init: Set[Cube] =
    (for {
      (l, y) <- data.zipWithIndex
      (c, x) <- l.zipWithIndex
      if c == '#'
    } yield Cube(x, y, 0))
    .toSet

  def cycle(active: Set[Cube]): Set[Cube] = {
    val inactive = active.flatMap(_.neighbors) -- active

    val remainActive = active.filter { pos =>
      val activeNs = pos.neighbors.count(active)
      activeNs == 2 || activeNs == 3
    }

    val newActive = inactive.filter { pos =>
      val activeNs = pos.neighbors.count(active)
      activeNs == 3
    }

    remainActive ++ newActive
  }

  var world = init
  for (i <- 1 to 6) {
    world = cycle(world)
    println(world.size)
  }
}

object Day17PartTwo extends App {
  println("Day 17 Part Two")

  val data = Files.readAllLines(Paths.get("src/day17/input.txt")).asScala.toIndexedSeq

  case class Cube(x: Int, y: Int, z: Int, w: Int) {
    def neighbors: Seq[Cube] =
      for {
        x2 <- x-1 to x+1
        y2 <- y-1 to y+1
        z2 <- z-1 to z+1
        w2 <- w-1 to w+1
        if !(x2 == x && y2 == y && z2 == z && w2 == w)
      } yield Cube(x2, y2, z2, w2)
  }

  val init: Set[Cube] =
    (for {
      (l, y) <- data.zipWithIndex
      (c, x) <- l.zipWithIndex
      if c == '#'
    } yield Cube(x, y, 0, 0))
      .toSet

  def cycle(active: Set[Cube]): Set[Cube] = {
    val inactive = active.flatMap(_.neighbors) -- active

    val remainActive = active.filter { pos =>
      val activeNs = pos.neighbors.count(active)
      activeNs == 2 || activeNs == 3
    }

    val newActive = inactive.filter { pos =>
      val activeNs = pos.neighbors.count(active)
      activeNs == 3
    }

    remainActive ++ newActive
  }

  var world = init
  for (i <- 1 to 6) {
    world = cycle(world)
    println(world.size)
  }
}
