package day02

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters._

object Day2 extends App {
  println("Day 2")
  val data = Files.readAllLines(Paths.get("src/day02/input.txt")).asScala.toSeq

  def isValid(l: String) = {
    val s"$minS-$maxS $charS: $password" = l
    val min = minS.toInt
    val max = maxS.toInt
    val char = charS.head
    password.count(_ == char) >= min && password.count(_ == char) <= max
  }

  val valid = data.filter(isValid)
  println(valid.length)
}

object Day2PartTwo extends App {
  println("Day 2 Part Two")
  val data = Files.readAllLines(Paths.get("src/day02/input.txt")).asScala.toSeq

  def isValid(l: String) = {
    val s"$pos1S-$pos2S $charS: $password" = l
    val pos1 = pos1S.toInt - 1
    val pos2 = pos2S.toInt - 1
    val char = charS.head
    password(pos1) == char ^ password(pos2) == char
  }

  val valid = data.filter(isValid)
  println(valid.length)
}

