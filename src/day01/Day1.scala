package day01

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters._

object Day1 extends App {
  println("Day 1")
  val numbers = Files.readAllLines(Paths.get("src/day01/input.txt")).asScala.toSeq.map(_.toInt)
  val answer = numbers.combinations(2)
    .collectFirst { case Seq(n1, n2) if n1 + n2 == 2020 => n1 * n2 }
    .get

  println(answer)
}

object Day1PartTwo extends App {
  println("Day 1 Part Two")
  val numbers = Files.readAllLines(Paths.get("src/day01/input.txt")).asScala.toSeq.map(_.toInt)
  val answer = numbers.combinations(3)
    .collectFirst { case Seq(n1, n2, n3) if n1 + n2 + n3 == 2020 => n1 * n2 * n3 }
    .get

  println(answer)
}
