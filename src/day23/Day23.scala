package day23

import java.time.{Duration, Instant}
import scala.annotation.tailrec

object Day23 extends App {
  println("Day 23")
  // val data = "389125467"  // example
  val data = "469217538"  // input
  val cups = data.map(_.toString.toInt).toList

  var move = 0
  var current = cups.head

  @tailrec def next(current: Int, pickUp: List[Int], rest: List[Int]): Unit = {
    move += 1
    if (move > 100) {
      println("\n-- final --")
      println(s"cups: ($current) ${pickUp.mkString(" ")} - ${rest.mkString(" ")}" )
      var total: List[Int] = current :: pickUp ::: rest
      total = total ::: total
      total = total.dropWhile(_ != 1).slice(1, 9)
      println(total.mkString)
    } else {
      println(s"\n-- move $move --")
      println(s"cups: ($current) ${pickUp.mkString(" ")} - ${rest.mkString(" ")}" )
      var dest = current
      while (!rest.contains(dest)) {
        dest -= 1
        if (dest == 0) dest = rest.max
      }
      println(s"destination: $dest")
      val (before, after) = rest.splitAt(rest.indexOf(dest)+1)
      var total: List[Int] = current :: before ::: pickUp ::: after
      total = total ::: total
      total = total.dropWhile(_ != current).drop(1)
      next(total.head, total.slice(1, 4), total.slice(4, 9))
    }
  }

  next(cups.head, cups.slice(1, 4), cups.drop(4))
}

object Day23PartTwo extends App {
  println("Day 23 Part Two")

  // val data = "389125467"  // example
  val data = "469217538"  // input
  val cups = data.map(_.toString.toInt).toVector
  val lookup = Array.ofDim[Node](1000001)

  class Node(val value: Int, var prev: Node, var next: Node) {
    def insertAfter(nodes: Node): Unit = {
      var lastNew = nodes
      while (lastNew.next != null) {
        lastNew = lastNew.next
      }
      lastNew.next = next
      next.prev = lastNew
      next = nodes
      nodes.prev = this
    }
    def insertAfter(value: Int): Node = {
      val n = new Node(value, this, next)
      lookup(value) = n
      next.prev = n
      next = n
      n
    }
    def remove(): Node = {
      prev.next = next
      next.prev = prev
      next
    }
    def take(n: Int): Node = {
      val start = this
      var end = this
      var i = 1
      while (i < n) {
        i += 1
        end = end.next
      }
      end.next.prev = start.prev
      start.prev.next = end.next
      start.prev = null
      end.next = null
      start
    }
  }

  var head: Node = new Node(0, null, null)
  head.prev = head
  head.next = head

  var cursor = head
  for (c <- cups) {
    cursor = cursor.insertAfter(c)
  }
  for (c <- Range.inclusive(10, 1000000)) {
    cursor = cursor.insertAfter(c)
  }

  cursor = head.remove()

  def printa(cursor: Node): Unit = {
    @tailrec def loop(n: Node): Unit = {
      print(s"${n.value} ")
      if (n.next != null && n.next != cursor) loop(n.next)
    }
    loop(cursor)
    println()
  }

  val maxValue = 1000000
  val start = Instant.now()
  var move = 0

  @tailrec def loop(current: Node): Unit = {
    move += 1
    if (move > 10000000) {
      println("\n-- final --")
      val after1 = lookup(1).next.take(40)
      printa(after1)
      println(s"${after1.value} * ${after1.next.value} = ${after1.value.toLong * after1.next.value.toLong} ")
    } else {
      //println(s"\n-- move $move --")
      //printa(current)
      val currentValue = current.value
      val pickUp = current.next.take(3)
      val pickUpValues = Set(pickUp.value, pickUp.next.value, pickUp.next.next.value)
      //println(s"cups: ($current) ${pickUp.mkString(" ")} - ${rest.mkString(" ")}" )
      var dest = currentValue
      do {
        dest -= 1
        if (dest == 0) dest = maxValue
      } while (pickUpValues.contains(dest))

      if (move % 100 == 0) {
        println()
        println(s"\n-- move $move ${move.toDouble / 100000.0} %  ${Duration.between(start, Instant.now)} --")
        println(s"current: $currentValue, destination: $dest")
      }
      val insertPos = lookup(dest)
      insertPos.insertAfter(pickUp)
      loop(current.next)
    }
  }

  loop(cursor)
}
