package day03

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters._

object Day3 extends App {
  println("Day 3")
  val data = Files.readAllLines(Paths.get("src/day03/input.txt")).asScala.toSeq

  val trees: Int = data.zipWithIndex.drop(1)
    .count { case (row, y) =>
      val x = (y * 3) % row.length
      row(x) == '#'
    }

  println(trees)
}

object Day3PartTwo extends App {
  println("Day 3 Part Two")
  val data = Files.readAllLines(Paths.get("src/day03/input.txt")).asScala.toVector
  val rowLength = data.head.length

  def isTree(x: Int, y: Int): Boolean =
    data(y)(x % rowLength) == '#'

  def count(dx: Int, dy: Int): Long = {
    var x, y = 0
    var res = 0
    while (y < data.length) {
      if (isTree(x, y)) res += 1
      x += dx
      y += dy
    }
    res
  }

  println(count(1,1))
  println(count(3,1))
  println(count(5,1))
  println(count(7,1))
  println(count(1,2))

  println(count(1,1) * count(3,1) * count(5,1) * count(7,1) * count(1,2))
}
