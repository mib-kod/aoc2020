package day25

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters._

object Day25 extends App {
  println("Day 25")
  val data = Files.readAllLines(Paths.get("src/day25/input.txt")).asScala.toSeq

  val cardPublicKey = data.head.toLong
  val doorPublicKey = data.last.toLong

  def transform(subjectNumber: Long, loopsize: Int): Long = {
    var v = 1L
    for (i <- 1 to loopsize) {
      v = v * subjectNumber
      v = v % 20201227
    }
    v
  }

  def findLoopsize(pubKey: Long): Int = {
    var v = 1L
    for (loopsize <- 1 to Int.MaxValue) {
      println(loopsize)
      v = v * 7
      v = v % 20201227
      if (v == pubKey)
        return loopsize
    }
    sys.error("Not found")
  }

  val cardLoopsize = findLoopsize(cardPublicKey)
  val doorLoopsize = findLoopsize(doorPublicKey)

  println()

  println(s"cardLoopsize=$cardLoopsize")
  println(s"doorLoopsize=$doorLoopsize")

  val encryptionKey1 = transform(doorPublicKey, cardLoopsize)
  println(s"encryptionKey1=$encryptionKey1")
  val encryptionKey2 = transform(cardPublicKey, doorLoopsize)
  println(s"encryptionKey2=$encryptionKey2")
}

object Day25PartTwo extends App {
  println("Day 25 Part Two")
  println("Den var lätt :)")
}
