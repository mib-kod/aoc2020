package day19

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day19 extends App {
  println("Day 19")
  val data = Files.readAllLines(Paths.get("src/day19/input.txt")).asScala.toSeq

  var (rules, messages) = data.span(_.nonEmpty)
  messages = messages.drop(1)

  sealed trait Rule
  case class CharRule(c: Char) extends Rule
  case class ComboRule(rs: Seq[Int]) extends Rule
  case class ComboOrRule(r1s: Seq[Int], r2s: Seq[Int]) extends Rule

  val allRules = (for (r <- rules) yield {
    val s"$no: $spec" = r
    val parsed: Rule = {
      if (spec == "\"a\"") CharRule('a')
      else if (spec == "\"b\"") CharRule('b')
      else if (spec.contains("|")) {
        val Array(a1, a2) = spec.split('|')
        ComboOrRule(a1.trim.split(' ').map(_.toInt), a2.trim.split(' ').map(_.toInt))
      } else ComboRule(spec.trim.split(' ').map(_.toInt))
    }
    no.toInt -> parsed
  }).toMap

  def toRegexp(r: Rule): String = r match {
    case CharRule(c) => c.toString
    case ComboRule(rs) => ((rs map allRules) map toRegexp).mkString("(", "", ")")
    case ComboOrRule(r1s, r2s) => "(" + ((r1s map allRules) map toRegexp).mkString + "|" + ((r2s map allRules) map toRegexp).mkString + ")"
  }

  val nollan = allRules(0)
  val regexp = toRegexp(nollan)

  var count = 0
  for (l <- messages) {
    if (l.matches(regexp)) {
      count += 1
      println(l)
    }
  }
  println(count)
}

object Day19PartTwo extends App {
  println("Day 19 Part Two")
  val data = Files.readAllLines(Paths.get("src/day19/input.txt")).asScala.toSeq

  var (rules, messages) = data.span(_.nonEmpty)
  messages = messages.drop(1)

  sealed trait Rule
  case class CharRule(c: Char) extends Rule
  case class ComboRule(rs: Seq[Int]) extends Rule
  case class ComboOrRule(r1s: Seq[Int], r2s: Seq[Int]) extends Rule

  val allRules = (for (r <- rules) yield {
    val s"$no: $spec" = r
    val parsed: Rule = {
      if (spec == "\"a\"") CharRule('a')
      else if (spec == "\"b\"") CharRule('b')
      else if (spec.contains("|")) {
        val Array(a1, a2) = spec.split('|')
        ComboOrRule(a1.trim.split(' ').map(_.toInt), a2.trim.split(' ').map(_.toInt))
      } else ComboRule(spec.trim.split(' ').map(_.toInt))
    }
    no.toInt -> parsed
  }).toMap ++ Map(
    8 -> ComboOrRule(Seq(42), Seq(42, 8)),
    11 -> ComboOrRule(Seq(42, 31), Seq(42, 11, 31)),
  )

  def toRegexp(r: Rule, depth: Int = 0): String = {
    if (depth == 20) ""
    else r match {
      case CharRule(c) => c.toString
      case ComboRule(rs) => ((rs map allRules) map { r => toRegexp(r, depth+1) }).mkString("(", "", ")")
      case ComboOrRule(r1s, r2s) =>
        "(" + ((r1s map allRules) map { r => toRegexp(r, depth+1) }).mkString + "|" +
              ((r2s map allRules) map { r => toRegexp(r, depth+1) }).mkString +
          ")"
    }
  }

  val nollan = allRules(0)
  val regexp = toRegexp(nollan)

  var count = 0
  for (l <- messages) {
    if (l.matches(regexp)) {
      count += 1
      println(l)
    }
  }
  println(count)
}
