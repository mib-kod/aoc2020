package day12

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day12 extends App {
  println("Day 12")
  val data = Files.readAllLines(Paths.get("src/day12/input.txt")).asScala.toSeq

  var x = 0
  var y = 0
  var cog = 90

  for (l <- data) {
    println(x, y, cog)
    val (inst, value) = l.splitAt(1)
    inst match {
      case "F" => cog match {
        case 0 => y -= value.toInt
        case 90 => x += value.toInt
        case 180 => y += value.toInt
        case 270 => x -= value.toInt
      }
      case "L" => cog -= value.toInt; if (cog < 0) cog += 360
      case "R" => cog += value.toInt; if (cog >=360) cog -= 360
      case "N" => y -= value.toInt
      case "S" => y += value.toInt
      case "E" => x += value.toInt
      case "W" => x -= value.toInt
    }
  }

  println(s"${x.abs} + ${y.abs} = ${x.abs + y.abs}")
}

object Day12PartTwo extends App {
  println("Day 12 Part Two")
  val data = Files.readAllLines(Paths.get("src/day12/input.txt")).asScala.toSeq

  var wE = 10
  var wN = 1
  var E = 0
  var N = 0

  for (l <- data) {
    println(E, N, wE, wN, l)
    val (inst, value) = l.splitAt(1)
    inst match {
      case "F" => E += wE * value.toInt; N += wN * value.toInt
      case "N" => wN += value.toInt
      case "S" => wN -= value.toInt
      case "E" => wE += value.toInt
      case "W" => wE -= value.toInt
      case "L" | "R" => l match {
        case "L90" | "R270" => val t = wE; wE = -wN; wN = t
        case "L180" | "R180" => wE = -wE; wN = -wN
        case "L270" | "R90" => val t = wE; wE = wN; wN = -t
      }
    }
  }

  // 77158 fel (för högt)
  // 46768 fel (för högt)

  // 11010 + 28130 = 39140 rätt

  println(s"${E.abs} + ${N.abs} = ${E.abs + N.abs}")
}
