package day22

import java.nio.file.{Files, Paths}
import scala.annotation.tailrec
import scala.collection.mutable
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day22 extends App {
  println("Day 22")
  val data = Files.readAllLines(Paths.get("src/day22/input.txt")).asScala.toSeq
  val player1 = data.takeWhile(_.nonEmpty).tail.map(_.toInt).to(mutable.Queue)
  val player2 = data.drop(data.indexOf("Player 2:")).tail.map(_.toInt).to(mutable.Queue)

  def calc(toSeq: Seq[Int]) = {
    val score = toSeq.reverse.zipWithIndex
      .map { case (value, idx) => value * (idx+1) }
      .sum

    println(score)
  }

  @tailrec def round(): Unit = {
    println(player1)
    println(player2)
    if (player1.isEmpty) {
      calc(player2.toSeq)
    } else if (player2.isEmpty) {
      calc(player1.toSeq)
    } else {
      val p1card = player1.dequeue()
      val p2card = player2.dequeue()
      if (p1card > p2card) {
        player1.enqueue(p1card)
        player1.enqueue(p2card)
        round()
      } else {
        player2.enqueue(p2card)
        player2.enqueue(p1card)
        round()
      }
    }
  }

  round()
}

object Day22PartTwo extends App {
  println("Day 22 Part Two")

  val data = Files.readAllLines(Paths.get("src/day22/input.txt")).asScala.toSeq
  val player1input = data.takeWhile(_.nonEmpty).tail.map(_.toInt)
  val player2input = data.drop(data.indexOf("Player 2:")).tail.map(_.toInt)

  def calc(toSeq: Seq[Int]): Long = {
    toSeq.map(_.toLong).reverse.zipWithIndex
      .map { case (value, idx) => value * (idx+1) }
      .sum
  }

  def game(gameNo: Int, player1Start: Seq[Int], player2Start: Seq[Int]): (Int, Long) = {
    val player1 = player1Start.to(mutable.Queue)
    val player2 = player2Start.to(mutable.Queue)

    val played = mutable.Set[(Seq[Int], Seq[Int])]()

    @tailrec def round(rond: Int): (Int, Long) = {
      println()
      println(s"-- Round $rond (Game $gameNo) --")
      println("Player 1's deck: " + player1.mkString(", "))
      println("Player 2's deck: " + player2.mkString(", "))
      val current = (player1.toSeq, player2.toSeq)
      if (played.contains(current)) {
        println("Loop win player 1")
        (1, 0)
      } else {
        played.add(current)
        if (player1.isEmpty) {
          (2, calc(player2.toSeq))
        } else if (player2.isEmpty) {
          (1, calc(player1.toSeq))
        } else {
          val p1card = player1.dequeue()
          println(s"Player 1 plays: $p1card")
          val p2card = player2.dequeue()
          println(s"Player 2 plays: $p2card")
          if (player1.length >= p1card && player2.length >= p2card) {
            println("Playing a sub-game to determine the winner...")
            val (winner, _ ) = game(gameNo+1, player1.toSeq.take(p1card), player2.toSeq.take(p2card))
            println(s"...anyway, back to game $gameNo.")
            if (winner == 1) {
              println(s"Player 1 wins round $rond of game $gameNo!")
              player1.enqueue(p1card)
              player1.enqueue(p2card)
              round(rond+1)
            } else {
              println(s"Player 2 wins round $rond of game $gameNo!")
              player2.enqueue(p2card)
              player2.enqueue(p1card)
              round(rond+1)
            }
          } else if (p1card > p2card) {
            println(s"Player 1 wins round $rond of game $gameNo!")
            player1.enqueue(p1card)
            player1.enqueue(p2card)
            round(rond+1)
          } else {
            println(s"Player 2 wins round $rond of game $gameNo!")
            player2.enqueue(p2card)
            player2.enqueue(p1card)
            round(rond+1)
          }
        }
      }
    }
    round(1)
  }

  val result = game(1, player1input, player2input)
  // 32027  Too low
  // 32629  Too low
  // 33670  Too high
  println(result)  // 33647  correct
}
