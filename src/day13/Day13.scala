package day13

import java.nio.file.{Files, Paths}
import scala.annotation.tailrec
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day13 extends App {
  println("Day 13")
  val data = Files.readAllLines(Paths.get("src/day13/input.txt")).asScala.toSeq
  val earliestTs = data.head.toInt
  val buses = data(1).split(',').toSeq.filterNot(_ == "x").map(_.toInt)

  @tailrec def loop(ts: Int): (Int, Int) = {
    buses.find(ts % _ == 0) match {
      case Some(bus) => (ts, bus)
      case None      => loop(ts + 1)
    }
  }
  val (ts, bus) = loop(earliestTs)
  val waitTime = ts - earliestTs
  println(s"$ts: $waitTime * $bus = ${waitTime * bus}")
}

object Day13PartTwo extends App {
  println("Day 13 Part Two")
  val data = Files.readAllLines(Paths.get("src/day13/input.txt")).asScala.toSeq
  val buses = data(1).split(',')

  val sorted = buses.zipWithIndex.collect { case (x, idx) if x != "x" => (x.toInt, idx) }
  val delta = sorted.head._2
  val codegen = for ((b, idx) <- sorted) yield {
    s"(ts+${idx-delta}) % $b == 0".replace("+-","-")
  }
  println(codegen.mkString(" && "))

  var found = false
  var ts = 294354277694157L

  // progressbar eftersom det tar sin lilla stund
  //val f = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate( () => println(". " + ts), 1, 1, TimeUnit.SECONDS)

  ts -= 510
  while (!found) {
    ts += 1
    found = (ts+0) % 19 == 0 && (ts+13) % 37 == 0 && (ts+19) % 383 == 0 && (ts+27) % 23 == 0 && (ts+32) % 13 == 0 && (ts+48) % 29 == 0 && (ts+50) % 457 == 0 && (ts+60) % 41 == 0 && (ts+67) % 17 == 0
  }
  println("FOUND " + ts)
  //f.cancel(true)

  // Rätt svar 294354277694107
}
