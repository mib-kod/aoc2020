package day15

import java.nio.file.{Files, Paths}
import scala.annotation.tailrec
import scala.collection.mutable
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day15 extends App {
  println("Day 15")
  val data = Files.readAllLines(Paths.get("src/day15/input.txt")).asScala.toSeq

  for (game <- data) {
    val numbers = game.split(',').map(_.toInt).toVector.reverse
    println(play(numbers))
  }

  @tailrec def play(input: Vector[Int]): Int = {
    if (input.length == 2020) input.head
    else {
      val next = input.drop(1).indexOf(input.head) match {
        case -1 => 0
        case idx => idx+1
      }
      play(next +: input)
    }
  }
}

object Day15PartTwo extends App {
  println("Day 15 Part Two")

  val data = Files.readAllLines(Paths.get("src/day15/input.txt")).asScala.toSeq

  for (game <- data) {
    val numbers = game.split(',').map(_.toInt).toVector
    println(play(numbers))
  }

  def play(input: Vector[Int]): Int = {
    val ns = new mutable.HashMap[Int, Int]()
    for ((v, idx) <- input.zipWithIndex) {
      ns.put(v, idx+1)
    }

    @tailrec def round(lastTurn: Int, lastValue: Int): Int = {
      //println(s"Turn $lastTurn: $lastValue")
      if (lastTurn == 30000000) lastValue
      else {
        val next = ns.get(lastValue) match {
          case Some(seenAt) => lastTurn - seenAt
          case None => 0
        }
        ns.put(lastValue, lastTurn)
        round(lastTurn + 1, next)
      }
    }

    round(input.length, input.last)
  }
}
