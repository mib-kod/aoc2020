package day07

import java.nio.file.{Files, Paths}
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day7 extends App {
  println("Day 7")
  val data = Files.readAllLines(Paths.get("src/day07/input.txt")).asScala.toSeq

  val combosB = List.newBuilder[(String, String)]
  for (line <- data) {
    val s"$container bags contain $bagsS." = line
    val bags = bagsS.split(", ").toSeq
    for (bag <- bags) {
      bag match {
        case "no other bags" =>
        case s"1 $color bag" =>
          combosB += color -> container
        case s"$amount $color bags" =>
          combosB += color -> container
      }
    }
  }
  val combos = combosB.result()
  println(combos.mkString("\n"))

  def find(color: String): List[String] = {
    combos.collect { case (k, v) if k == color => v } match {
      case Nil => Nil
      case xs => ((xs flatMap find) ::: xs).distinct
    }
  }

  val result = find("shiny gold")
  println(result.length)
}

object Day7PartTwo extends App {
  println("Day 7 Part Two")

  val data = Files.readAllLines(Paths.get("src/day07/input.txt")).asScala.toSeq

  val combosB = List.newBuilder[(String, String)]
  for (line <- data) {
    val s"$container bags contain $bagsS." = line
    for (bag <- bagsS.split(", ")) {
      bag match {
        case "no other bags" =>
        case s"1 $color bag" =>
          combosB += container -> color
        case s"$amount $color bags" =>
          for (_ <- 1 to amount.toInt)
            combosB += container -> color
      }
    }
  }

  val combos = combosB.result()
  println(combos.mkString("\n"))

  def find(color: String): List[String] = {
    combos.collect { case (k, v) if k == color => v } match {
      case Nil => Nil
      case xs => ((xs flatMap find) ::: xs)
    }
  }

  val result = find("shiny gold")
  println(result)
  println(result.length)
}
