package day20

import java.io.File
import java.nio.file.{Files, Paths}
import scala.collection.mutable
import scala.jdk.CollectionConverters.CollectionHasAsScala

object Day20 extends App {
  println("Day 20")
  val data = Files.readAllLines(Paths.get("src/day20/input.txt")).asScala.toSeq

  def spanMany[T](xs: Seq[T], p: T => Boolean): Seq[Seq[T]] = xs.span(!p(_)) match {
    case (first, Nil) => first :: Nil
    case (first, rest) => first +: spanMany(rest.tail, p)
  }

  case class Tile(id: Int, data: Array[Array[Char]]) {
    var maybeNeighbors: Set[Tile] = Set.empty

    def print: String =
      s"*$id*\n" + data.map(_.mkString(" ")).mkString("\n").replace("#","▓▓").replace(".","░░")

    override def toString: String =
      s"*$id*"

    def n: String = data.head.mkString
    def s: String = data.last.mkString
    def w: String = data.map(_.head).mkString
    def e: String = data.map(_.last).mkString
    def toInt(s: String): Int = Integer.parseInt(s.replace('.','0').replace('#','1'), 2)

    val edges: Set[Int] = Set(
      toInt(n), toInt(s), toInt(w), toInt(e), toInt(n.reverse), toInt(s.reverse), toInt(w.reverse), toInt(e.reverse)
    )
  }

  val tiles = for (group <- spanMany[String](data, _.isBlank)) yield {
    val s"Tile $d:" = group.head
    val data = group.tail.toArray.map(_.toCharArray)
    Tile(d.toInt, data)
  }

  for (t <- tiles)
    t.maybeNeighbors = tiles.filterNot(_ == t).filter(_.edges.exists(t.edges)).toSet

  val corners = tiles.sortBy(_.maybeNeighbors.size).take(4).map(_.id.toLong)
  println(s"${corners.mkString(" * ")} = ${corners.product}")
}

object Day20PartTwo extends App {
  println("Day 20 Part Two")

  val data = Files.readAllLines(Paths.get("src/day20/input.txt")).asScala.toSeq

  def spanMany[T](xs: Seq[T], p: T => Boolean): Seq[Seq[T]] = xs.span(!p(_)) match {
    case (first, Nil) => first :: Nil
    case (first, rest) => first +: spanMany(rest.tail, p)
  }

  case class Tile(id: Int, var data: Array[Array[Char]]) {
    var maybeNeighbors: Set[Tile] = Set.empty

    def isCorner = maybeNeighbors.size == 2
    def isEdge = maybeNeighbors.size == 3
    def isInner = maybeNeighbors.size > 3

    def print: String =
      s"*$id*\n${data.head.mkString}\n\n" + data.map(_.mkString(" ")).mkString("\n").replace("#","▓▓").replace(".","░░")

    override def toString: String =
      s"$id"

    def rotate(): Unit = data = data.transpose
    def flipY(): Unit = data = data.reverse
    def flipX(): Unit = data = data.map(_.reverse)

    def toInt(s: String): Int = Integer.parseInt(s.replace('.','0').replace('#','1'), 2)
    def n: Int = toInt(data.head.mkString)
    def s: Int = toInt(data.last.mkString)
    def w: Int = toInt(data.map(_.head).mkString)
    def e: Int = toInt(data.map(_.last).mkString)
    def nR: Int = toInt(data.head.mkString.reverse)
    def sR: Int = toInt(data.last.mkString.reverse)
    def wR: Int = toInt(data.map(_.head).mkString.reverse)
    def eR: Int = toInt(data.map(_.last).mkString.reverse)

    def transformToSouth(expected: Int): Unit = {
      if (s == expected) return
      flipX()
      if (s == expected) return
      flipY()
      if (s == expected) return
      rotate()
      if (s == expected) return
      flipX()
      if (s == expected) return
      flipY()
    }

    def transformToNorth(expected: Int): Unit = {
      if (n == expected) return
      flipX()
      if (n == expected) return
      flipY()
      if (n == expected) return
      flipX()
      if (n == expected) return
      rotate()
      if (n == expected) return
      flipX()
      if (n == expected) return
      flipY()
      if (n == expected) return
      flipX()
      if (n == expected) return
      sys.error("Could not match")
    }

    def transformToWest(expected: Int): Unit = {
      if (w == expected) return
      flipX()
      if (w == expected) return
      flipY()
      if (w == expected) return
      flipX()
      if (w == expected) return
      rotate()
      if (w == expected) return
      flipX()
      if (w == expected) return
      flipY()
      if (w == expected) return
      flipX()
      if (w == expected) return
      sys.error("Could not match")
    }

    def transformToNorthWest(expectedNorth: Int, expectedWest: Int): Unit = {
      if (n == expectedNorth && w == expectedWest) return
      flipX()
      if (n == expectedNorth && w == expectedWest) return
      flipY()
      if (n == expectedNorth && w == expectedWest) return
      flipX()
      if (n == expectedNorth && w == expectedWest) return
      rotate()
      if (n == expectedNorth && w == expectedWest) return
      flipX()
      if (n == expectedNorth && w == expectedWest) return
      flipY()
      if (n == expectedNorth && w == expectedWest) return
      flipX()
      if (n == expectedNorth && w == expectedWest) return
      sys.error("Could not match")
    }

    val edges: Set[Int] =
      Set(n,s,e,w,nR,sR,eR,wR)
  }

  val tiles = for (group <- spanMany[String](data, _.isBlank)) yield {
    val s"Tile $d:" = group.head
    val data = group.tail.toArray.map(_.toCharArray)
    Tile(d.toInt, data)
  }

  for (t <- tiles.sortBy(_.id)) {
    t.maybeNeighbors = tiles.filterNot(_ == t).filter(_.edges.exists(t.edges)).toSet
  }

  for (t <- tiles.sortBy(_.maybeNeighbors.size)) {
    println(t.id + " " + t.edges + " " + t.maybeNeighbors.map(_.id))
  }

  val corners: Seq[Tile] = tiles.filter(_.isCorner)
  val edges: Seq[Tile] = tiles.filter(_.isEdge)
  val inner: Seq[Tile] = tiles.filter(_.isInner)

  println("\nCORNERS:")
  println(corners)
  println("\nEDGES:")
  println(edges)
  println("\nINNER:")
  println(inner)

  class Board2 {
    val w: Int = math.sqrt(tiles.length).intValue()

    class BoardPosition(val x: Int, val y: Int) {
      var placed: Option[Tile] = None
      def isEmpty = placed.isEmpty
      val candidates = mutable.Queue[Tile]()

      def north = if (y == 0) null else grid(x)(y-1)
      def east  = if (x == w-1) null else grid(x+1)(y)
      def south = if (y == w-1) null else grid(x)(y+1)
      def west  = if (x == 0) null else grid(x-1)(y)

      def neighbors = Seq(north, east, south, west).filterNot(_ == null)

      def isCorner = x == 0 && y == 0 || x == w-1 && y == 0 || x == 0 && y == w-1 || x == w-1 && y == w-1
      def isEdge = !isCorner && (x == 0 || y == 0 || x == w-1 || y == w-1)
      def isInner = x > 0 && x < w-1 && y > 0 && y < w-1

      def connects(other: Tile): Boolean = placed match {
        case Some(tile) => tile != other && tile.edges.exists(other.edges)
        case None => true
      }

      def place(t: Tile): Boolean = {
        if (isEmpty && (t.isInner == isInner) && (t.isCorner == isCorner) && (t.isEdge == isEdge) && neighbors.forall(_.connects(t))) {
          placed = Some(t)
          for (n <- neighbors if n.isEmpty) {
            n.candidates.clear()
            n.candidates.enqueueAll(t.maybeNeighbors)
          }
          true
        } else {
          false
        }
      }

      def placeCandidate(): Boolean = {
        var found = false
        while (!found && candidates.nonEmpty) {
          found = place(candidates.dequeue())
        }
        found
      }

      override def toString =
        s"($x,$y)"

      def render = {
        val sb = new StringBuilder()
        if (isCorner) sb += 'C'
        if (isEdge) sb += 'E'
        if (isInner) sb += 'I'
        placed match {
          case Some(t) => sb.append("*").append(t.id).append("* ")
          case None => sb.append(" null ")
        }
        sb.append(candidates.length)
        sb.append("                           ")
        sb.result()
      }
    }

    val grid: Array[Array[BoardPosition]] = Array.ofDim(w, w)
    for (x <- 0 until w; y <- 0 until w) {
      grid(x)(y) = new BoardPosition(x, y)
    }

    def printIt(): Unit = {
      for (y <- 0 until w) {
        for (x <- 0 until w) {
          print(grid(x)(y).render.take(20) + " | ")
        }
        println()
      }
    }

    def image(): String = {
      val sb = new StringBuilder()
      for (y <- 0 until w) {
        for (l <- 1 until 9) {
          for (x <- 0 until w) {
            for (c <- 1 until 9) {
              sb.append(grid(x)(y).placed.get.data(l)(c))
            }
          }
          sb.append('\n')
        }
      }
      sb.result()
    }

    def place(x: Int, y: Int, tile: Tile): Unit = {
      grid(x)(y).place(tile)
    }

    def search(): Boolean = {
      var backtrack: List[BoardPosition] = Nil
      var count = 0
      while (true) {
        count += 1
        grid.iterator.flatten.find(_.isEmpty) match {
          case None =>
            printIt()
            println("FOUND!!")
            return true
          case Some(head) =>
            val goon = head.placeCandidate()
            if (goon) {
              backtrack = head :: backtrack
            } else if (backtrack.nonEmpty) {
              val back :: rest = backtrack
              backtrack = rest
              back.placed = None
            } else {
              println("Not found :(")
              return false
            }
        }
      }
      ???
    }
  }

  def findBoard(): Board2 = {
    var x = 0
    for (cornerAlt <- corners.permutations) {
      x+=1
      println("\n\n********************************************************")
      val b = new Board2
      b.place(0, 0, cornerAlt(0))
      b.place(0, b.w-1, cornerAlt(1))
      b.place(b.w-1, 0, cornerAlt(2))
      b.place(b.w-1, b.w-1, cornerAlt(3))
      b.printIt()
      val found = b.search()
      if (found) return b
    }
    ???
  }

  val board = findBoard()

  println("searchSemonster")
  board.printIt()

  locally {
    // Vänd översta två rutorna
    val north = board.grid(0)(0).placed.get
    val south = board.grid(0)(1).placed.get
    val common = north.edges intersect south.edges
    println(common)
    north.transformToSouth(common.last)
    south.transformToNorth(common.last)

// För example.txt
//    north.flipX()
//    south.flipX()
//    south.transformToNorth(common.last)
//    north.transformToSouth(common.last)
  }

  // Vänd alla rutor längs vänsterkanten
  for (y <- 1 until board.w) {
    val north = board.grid(0)(y-1).placed.get
    val south = board.grid(0)(y).placed.get
    south.transformToNorth(north.s)
  }

  // Vänd alla rutor längs överkanten
  for (x <- 1 until board.w) {
    val west = board.grid(x-1)(0).placed.get
    val east = board.grid(x)(0).placed.get
    east.transformToWest(west.e)
  }

  // och slutligen resten
  for (y <- 1 until board.w; x <- 1 until board.w) {
    val north = board.grid(x)(y-1).placed.get
    val west = board.grid(x-1)(y).placed.get
    val current = board.grid(x)(y).placed.get
    current.transformToNorthWest(north.s, west.e)
  }

  var image: Array[Array[Char]] = board.image().trim().split('\n').map(_.toCharArray)

  // image = image.transpose  för example txt
  image = image.map(_.reverse)

// Generera monster-detection-if-satsen
//  val monster =
//    """|                  #
//       |#    ##    ##    ###
//       | #  #  #  #  #  #   """.stripMargin
//
//  println(monster.linesIterator.toArray.apply(1).zipWithIndex.filter { case (c, idx) => c == '#' }.map{ case (c, idx) => s"image(y+1)(x+$idx) == '#' &&" }.mkString("\n"))
//  println(monster.linesIterator.toArray.apply(2).zipWithIndex.filter { case (c, idx) => c == '#' }.map{ case (c, idx) => s"image(y+2)(x+$idx) == '#' &&" }.mkString("\n"))

  def monsterAtPos(x: Int, y: Int): Boolean = {
    image(y)(x+18) == '#' &&
    image(y+1)(x+0) == '#' &&
    image(y+1)(x+5) == '#' &&
    image(y+1)(x+6) == '#' &&
    image(y+1)(x+11) == '#' &&
    image(y+1)(x+12) == '#' &&
    image(y+1)(x+17) == '#' &&
    image(y+1)(x+18) == '#' &&
    image(y+1)(x+19) == '#' &&
    image(y+2)(x+1) == '#' &&
    image(y+2)(x+4) == '#' &&
    image(y+2)(x+7) == '#' &&
    image(y+2)(x+10) == '#' &&
    image(y+2)(x+13) == '#' &&
    image(y+2)(x+16) == '#'
  }

  def drawMonster(x: Int, y: Int): Unit = {
    image(y)(x+18) = 'O'
    image(y+1)(x+0) = 'O'
    image(y+1)(x+5) = 'O'
    image(y+1)(x+6) = 'O'
    image(y+1)(x+11) = 'O'
    image(y+1)(x+12) = 'O'
    image(y+1)(x+17) = 'O'
    image(y+1)(x+18) = 'O'
    image(y+1)(x+19) = 'O'
    image(y+2)(x+1) = 'O'
    image(y+2)(x+4) = 'O'
    image(y+2)(x+7) = 'O'
    image(y+2)(x+10) = 'O'
    image(y+2)(x+13) = 'O'
    image(y+2)(x+16) = 'O'
  }

  for (x <- 0 until image.length-18; y <- 0 until image.length-1) {
    if (monsterAtPos(x, y)) {
      println(s"x=$x y=$y  MONSTER!")
      drawMonster(x, y)
    }
  }

  val roughness = image.flatten.count(_ == '#')
  println(roughness)

  Files.writeString(new File("day20.html").toPath, html(board, image.map(_.mkString).mkString("\n")).toString)

  // Skapa en enkel webbsida för att visualisera hur bilderna vänds för enklare debugging
  def html(board: Board2, image: String) = {
    <html>
      <head>
        <meta charset="UTF-8"/>
      </head>
      <body>
      <h1>Alla delbilder</h1>
      <table border="1"> {
        for (y <- 0 until board.w) yield {
        <tr> {
          for (x <- 0 until board.w) yield {
          <td><pre style="font-family: Arial; line-height: 23px;" >{board.grid(x)(y).placed.get.print}</pre></td>
          } }
        </tr>
        } }
      </table>
      <h1>Sammansatt</h1>
      <pre style="font-family: Arial; line-height: 23px;" >{board.image().replace("#","▓▓").replace(".","░░")}</pre>
      <h1>Monster</h1>
      <pre style="font-family: Arial; line-height: 23px;" >{image.replace("#","▓▓").replace(".","░░").replace("O","OO")}</pre>
    </body>
    </html>
  }
}
