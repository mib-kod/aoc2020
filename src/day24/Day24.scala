package day24

import java.nio.file.{Files, Paths}
import scala.annotation.tailrec
import scala.collection.mutable
import scala.jdk.CollectionConverters._

object Day24 extends App {
  println("Day 24")
  val data = Files.readAllLines(Paths.get("src/day24/input.txt")).asScala.toSeq

  case class Coord(x: Int, y: Int) {
    def e()  = Coord(x+1, y)
    def se() = Coord(x+1, y-1)
    def sw() = Coord(x,   y-1)
    def w()  = Coord(x-1, y)
    def nw() = Coord(x-1, y+1)
    def ne() = Coord(x,   y+1)
  }

  def tile(s: String) = {
    @tailrec def loop(path: List[Char], coord: Coord): Coord = path match {
      case Nil => coord
      case 'e'        :: rest => loop(rest, coord.e())
      case 's' :: 'e' :: rest => loop(rest, coord.se())
      case 's' :: 'w' :: rest => loop(rest, coord.sw())
      case 'w'        :: rest => loop(rest, coord.w())
      case 'n' :: 'w' :: rest => loop(rest, coord.nw())
      case 'n' :: 'e' :: rest => loop(rest, coord.ne())
    }
    loop(s.toList, Coord(0,0))
  }

  val black = mutable.Set[Coord]()

  for (path <- data) {
    val t: Coord = tile(path)
    if (black.contains(t))
      black.remove(t)
    else
      black.add(t)
  }

  println(black.size)
}

object Day24PartTwo extends App {
  println("Day 24 Part Two")

  val data = Files.readAllLines(Paths.get("src/day24/input.txt")).asScala.toSeq

  case class Coord(x: Int, y: Int) {
    def e()  = Coord(x+1, y)
    def se() = Coord(x+1, y-1)
    def sw() = Coord(x,   y-1)
    def w()  = Coord(x-1, y)
    def nw() = Coord(x-1, y+1)
    def ne() = Coord(x,   y+1)

    def neighbors: Set[Coord] = Set(e(), se(), sw(), w(), nw(), ne())
  }

  def tile(s: String) = {
    @tailrec def loop(path: List[Char], coord: Coord): Coord = path match {
      case Nil => coord
      case 'e'        :: rest => loop(rest, coord.e())
      case 's' :: 'e' :: rest => loop(rest, coord.se())
      case 's' :: 'w' :: rest => loop(rest, coord.sw())
      case 'w'        :: rest => loop(rest, coord.w())
      case 'n' :: 'w' :: rest => loop(rest, coord.nw())
      case 'n' :: 'e' :: rest => loop(rest, coord.ne())
    }
    loop(s.toList, Coord(0,0))
  }

  val black = mutable.Set[Coord]()

  for (path <- data) {
    val t: Coord = tile(path)
    if (black.contains(t))
      black.remove(t)
    else
      black.add(t)
  }

  def dayFlip(): Unit = {
    val flipToWhite = black.toSet
      .filter { x => x.neighbors.intersect(black).isEmpty || x.neighbors.intersect(black).size > 2 }

    val flipToBlack: Seq[Coord] = black.toSeq.flatMap(_.neighbors)
      .filterNot(black)
      .filter { x => x.neighbors.intersect(black).size == 2 }

    black ++= flipToBlack
    black --= flipToWhite
  }

  for (day <- 1 to 100) {
    dayFlip()
    println(s"Day: $day: ${black.size}")
  }
}
